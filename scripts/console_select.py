#!/usr/bin/env python3
import pexpect
import time
import sys

# the script takes an argument for console connection to the switch
# e.g "ssh 10.90.2.2 -p 7027"

def onyx_grub_menu(conn):
    KEY_DOWN = '\033[B'
    print('Waiting for GNU GRUB to show')
    conn.expect_exact("GNU GRUB", timeout=480)
    time.sleep(1)
    conn.expect_exact('Use the ^ and v keys')
    time.sleep(1)
    #
    conn.expect_exact('ONIE')
    # Onyx Boot 1
    conn.send(KEY_DOWN)
    # Onyx Boot 2
    conn.send(KEY_DOWN)
    # ONIE Uninstall
    time.sleep(10)
    # Hit Enter
    conn.sendline('')
    # time.sleep(10)
    # conn.send(KEY_DOWN)
    # conn.sendcontrol('m')
    conn.sendline('YES')
    time.sleep(1)
    conn.sendline('')
    # ONIE uninstall MUST be done to get standard ONIE grub menu.

def onie_grub_menu(conn):
    KEY_UP = '\033[A'
    KEY_DOWN = '\033[B'
    print ('Waiting for GNU GRUB to show')
    conn.expect_exact("GNU GRUB", timeout=480)
    time.sleep(1)
    conn.expect_exact('Use the ^ and v keys')
    time.sleep(1)
    #
    conn.expect_exact('ONIE')
    # going all the way to the top then move one level down to onie-rescue mode
    conn.send(KEY_UP)
    conn.send(KEY_UP)
    conn.send(KEY_UP)
    conn.send(KEY_UP)
    conn.send(KEY_DOWN)

    time.sleep(10)
    # Hit Enter
    conn.sendline('')
    conn.sendline('')


def main():
    print(sys.argv[1])
    connection = pexpect.spawn(str(sys.argv[1]))
   # reboot box
    onyx_grub_menu(connection)
    onie_grub_menu(connection)

main()
